---
title: "Shiny application and reproducible pitch. Esophageal Cancer prediction"
author: "D. Otryakhin"
highlighter: highlight.js
output: html_document
job: null
knit: slidify::knit2slides
mode: selfcontained
hitheme: tomorrow
subtitle: slides accompanying the software
framework: io2012
widgets: []
---

## Introduction

* The main idea is to teach a machine learning algorithm to predict esophageal cancer using esoph data, so that regular people will be able to calculate their probability to have such cancer.

* The algorithm is a general linear regression model with 10-fold cross-validation.

* The data used is esoph dataset from datasets package.



```r
data(esoph)
head(esoph,4)
```

```
##   agegp     alcgp    tobgp ncases ncontrols
## 1 25-34 0-39g/day 0-9g/day      0        40
## 2 25-34 0-39g/day    10-19      0        10
## 3 25-34 0-39g/day    20-29      0         6
## 4 25-34 0-39g/day      30+      0         5
```

--- 

## Summary of the data


```r
summary(esoph)
```

```
##    agegp          alcgp         tobgp        ncases         ncontrols    
##  25-34:15   0-39g/day:23   0-9g/day:24   Min.   : 0.000   Min.   : 1.00  
##  35-44:15   40-79    :23   10-19   :24   1st Qu.: 0.000   1st Qu.: 3.00  
##  45-54:16   80-119   :21   20-29   :20   Median : 1.000   Median : 6.00  
##  55-64:16   120+     :21   30+     :20   Mean   : 2.273   Mean   :11.08  
##  65-74:15                                3rd Qu.: 4.000   3rd Qu.:14.00  
##  75+  :11                                Max.   :17.000   Max.   :60.00
```



---
## The model


```r
Usedata<-esoph
index<-createDataPartition(y=Usedata$p, p=0.8, list=F)
set_training<-Usedata[index,]
set_testing<-Usedata[-index,]
Lmodel<-train(p ~ agegp + tobgp * alcgp, data=set_training,
      method="glm", trControl = trainControl(method="cv"))
Lmodel
```

```
## Generalized Linear Model 
## 
## 72 samples
##  5 predictor
## 
## No pre-processing
## Resampling: Cross-Validated (10 fold) 
## Summary of sample sizes: 65, 65, 66, 64, 64, 65, ... 
## Resampling results
## 
##   RMSE       Rsquared   RMSE SD     Rsquared SD
##   0.2770769  0.4583778  0.06627007  0.2097024  
## 
## 
```

---

## Algorithm performance on a test set

* The first figure shows the real probability 

![plot of chunk unnamed-chunk-6](assets/fig/unnamed-chunk-6-1.png)

* The next one is the predicted probability. The performance is obviously not good.

![plot of chunk unnamed-chunk-7](assets/fig/unnamed-chunk-7-1.png)

